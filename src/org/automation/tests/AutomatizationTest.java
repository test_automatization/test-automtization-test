package org.automation.tests;

import org.automation.automatization.Automatization;
import org.automation.automatization.AutomatTestException;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class AutomatizationTest {

    Automatization automatization = new Automatization("https://hup.hu/");

    @Test
    public void TestLoginForm() {
        assertEquals(automatization.isLoginAvailable(), true);
    }


    @Test
    public void TestNumberOfCommenters() throws AutomatTestException {
        List<String> result = automatization.listCommenters();

        assertEquals(result.size() > 10 && result.size() < 100, true);
    }

    @Test
    public void TestProhibitedCommenter() throws AutomatTestException {
        List<String> result = automatization.listCommenters();

        assertEquals(result.contains("trey"), false);
    }


}
