package org.automation.automatization;

public class AutomatTestException extends Exception {

    private String message;

    public AutomatTestException(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "AutomatTestException{" +
                "Error during testing the webpage:'" + message + '\'' +
                '}';
    }
}
