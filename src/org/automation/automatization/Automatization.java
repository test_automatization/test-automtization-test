package org.automation.automatization;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Automatization {

    private WebDriver driver;

    public Automatization(String url) {
        // Use Chrome
        // Setup driver binary
        System.setProperty("webdriver.chrome.driver", "res/chromedriver.exe");
        // Setup Chrome driver
        driver = new ChromeDriver();

        driver.manage().window().maximize();

        driver.get(url);
    }

    public List<String> listCommenters() throws AutomatTestException, NoSuchElementException {

        List<WebElement> lstTrElement = driver.findElements(By.xpath("//*[@id=\"block-block-16\"]/div/table/tbody/tr/td[2]/a"));

        String[] urls = new String[0]; // első 10 fórum topic url-je
        urls = new String[10];

        try {
            for (int i = 0; i < 10; i++) {
                urls[i] = lstTrElement.get(i).getAttribute("href");
            }
        } catch (Exception e) {
            throw new AutomatTestException("Cannot get first 10 Urls: " + e);
        }

        List<String> submitted = new ArrayList<>();

        for (String s : urls) {

            driver.get(s);

            List<WebElement> temp = driver.findElements(By.cssSelector("#comments>.comment>.submitted"));

            if (temp.size() > 10) {
                temp = temp.subList(0, 10);
            }

            try {
                for (WebElement e : temp) {
                    if (temp != null && temp.size() != 0) {
                        String[] splitted = e.getText().split("\\|");
                        String currentName = splitted[0].substring(2).trim();
                        if (!submitted.contains(currentName)) {
                            submitted.add(currentName);
                        }
                    } else throw new AutomatTestException("Commenters list is empty");
                }
            } catch (Exception e) {
                throw new AutomatTestException("Cannot list the Commenters: " + e);
            }
        }

        Collections.sort(submitted, Collator.getInstance());

        for (String s : submitted) {
            System.out.println(s);
        }

        driver.quit();

        return submitted;
    }


    public boolean isLoginAvailable() throws NoSuchElementException {

        List<WebElement> username = driver.findElements(By.xpath("//*[@id=\"edit-name\"]"));
        List<WebElement> password = driver.findElements(By.xpath("//*[@id=\"edit-pass\"]"));

        if (username.size() == 1 && password.size() == 1) {
            driver.quit();
            return true;
        }
        driver.quit();
        return false;
    }

    public static void main(String[] args) {

        Automatization automatization = new Automatization("https://hup.hu/");

        try {
            automatization.listCommenters();
        } catch (AutomatTestException e) {
            e.printStackTrace();
        }


    }

}
